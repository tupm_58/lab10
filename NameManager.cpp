#include<iostream>
#include "NameManager.h"
NameManager::NameManager():
	name()
{}
NameManager::NameManager(const NameManager & nm){
	name = nm.name;
}
void NameManager::add(string _name){//cho biet them ten danh danh sach nay neu no khong phai la da hien dien
	getline(cin, _name);
	if(name.size() < name.max_size() ){
		name.push_back(_name);
	}
	else cout << "Danh sach day!" << endl;
}
void NameManager::remove(int index){//loai bo ten o vi tri quy dinh trong danh sach
	if(index > 0 && index < size() ){
		name.erase(name.begin() + index);
	}
	else cout << "Khong ton tai! " << endl;
}
int NameManager::indexOf(string _name){//Tra ve chi so cua su xuat hien dau ti�n cua quy dinh 
// Ten trong danh sach nay, hoac -1 neu danh sach nay khong co ten
	int i = 0;
	while(i < size() ){
		if( _name == name.at(i) ) return i;
		i++;
	}
	return -1;
}
string NameManager::get(int index){//tra ve ten o vi tri quy dinh trong danh sach nay
	if( index > 0 && index < size() ) return name.at(index);
}
int NameManager::size(){// tra ve so ten trong danh sach nay
	return (int) name.size();	
}
void NameManager::sort(){// sap xep cAc ten trong danh sach nay vao thu tu chu cai
	int i, j;
	for(i = 0; i < size(); i++){ 
		for(j = i + 1; j < size(); j++){ 
			if( name[i] > name[j] ) name[i].swap(name[j]);
		}
	}
}
void NameManager::print(){//hien thi tat ca cac ten trong danh sach nay
	int i;
	for(i = 0; i < size(); i++){
		cout << name[i] << endl;
	}
}
