#ifndef NAMEMANAGER
#define NAMEMANAGER
#include<iostream>
#include<cstring>
#include <vector>
using namespace std;
const int MAX=100;

class NameManager {
	private: 
	vector<string> name;
	public: 
	NameManager(); // default constructor 
	NameManager (const NameManager & nm);  // copy constructor   
	void add(string name); // adds the specified name to this list if it is not already present 
	void remove(int index); // removes the name at the specified position in this list 
	int indexOf(string name); // returns the index of the first occurrence of the specified     
	 // name in this list, or -1 if this list does not contain the name 
	int size(); // returns the number of names in this list 
	string get(int index);  // returns the name at the specified position in this list 
	void sort(); // sorts the names in this list into alphabetical order 
	void print(); // displays all the names in this list 
}; 
#endif
