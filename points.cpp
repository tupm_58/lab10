#include<iostream>
#include<cmath> 
#include<fstream>
#include <iomanip> 
const int MAX = 100;
using namespace std;

class Point{ 
private:
	double x;
	double y;
public: 
	Point(); // default constructor 
	Point(double x, double y); // constructor from coordinates x, y 
	Point(const Point& p);  // copy constructor   
	/* Functions to access the state of the object */ 
	double getX() const; // gets x coordinate 
	double getY() const; // gets y coordinate 
	void setX(double _x);
	void setY(double _y);
	double dist(const Point& p); // computes the distance to another point 
};  
Point::Point() : 
	x(0), y(0)
{}
Point::Point(double _x, double _y): 
	x(_x), y(_y)
{}
Point::Point(const Point& p){ 
	x = p.x;
	y = p.y;
}
double Point::getX() const{
	return x;
}
double Point::getY() const{
	return y;
}

void Point::setY(double _y) {
	y = _y;
}

void Point::setX(double _x) { 
	x = _x;
}
double Point::dist(const Point& p){
	return sqrt((x - p.getX()) * (x - p.getX()) + (y - p.getY()) * (y - p.getY()));
}
int main(){
	int x, y, i, j, number; 
	ifstream inp;
	ofstream out;
	inp.open("Points.inp");
	out.open("Points.out");
	
	inp >> number;
	Point p[number];
	
	for(i = 0; i < number; i++){
		inp >> x;
		inp >> y;
		p[i].setX(x);
		p[i].setY(y);
	}
	
	for(i = 0; i < number; i++){
		for(j = 0; j < number; j++){
			if(j == i) continue;
			double d = p[i].dist(p[j]);
			cout << fixed << setprecision(2)<< d << "\t";
			out << fixed << setprecision(2) << d << "\t";
		}
		cout << endl;
		out << endl;
	}
	inp.close();
	out.close();
	return 0;
}
